import csv
import urllib
from GoogleUtilities.GooglePhotosClient import GooglePhotosClient
from datetime import datetime
import os
import gdata.client


class PhotoCrawler(object):
    def __init__(self,
                 config, buffer_dir):
        self.client_info = config['client']
        self.csv_path = config['files']['csv']
        self.buffer_dir = buffer_dir
        self.program_info = config['program']
        self.photoClient = GooglePhotosClient(config)
        self.photoClient.authorize()
        self.photoClient.refresh()
    # def getLinks(self, fp):
    #     with open(fp, 'r') as f:
    #         Reader = csv.DictReader(f)
    #
    #         print('Group: ' + str(self.client_info['group']))
    #         return [(i['Album Name'], i['Quality'], i['Performance']) for i in Reader if
    #                 i['Group'] == str(self.client_info['group'])]

    def getImage(self,
                 dir_name,
                 item):
            album_name,quality,performance = item
            paths = []
            if (self.client_info['type'] == "master"):
                paths = [("snap", quality), ("auto", performance)]
            elif (self.client_info['type'] == 'worker'):
                paths = [("auto", performance)]
            return_fp = []
            for j in paths:
                attr, link = j
                now = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
                fp = os.path.join(os.path.abspath(dir_name), album_name + "_" + now + "_" + attr + ".jpg")
                try:
                    urllib.urlretrieve(link, fp)
                    return_fp.append(fp)
                except Exception, Error:
                    print Error
                    print ("link: " + link)
                    pass

    def iterativeCrawl(self, links):
        # Purposes: Download a batch of images locally into dir name, and perform batch upload
        while True:
            for i in links:
                self.getImage(self.buffer_dir, i)

    def iterativeUpload(self, album_name):
        #traverse the directory for upload
        self.photoClient.get_albums()
        while (True):
            self.photoClient.authorize()
            now = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
            msg = "Uploaded by " + self.client_info['name'] + "\r\nUpload Time: " + now
            self.photoClient.batchUploadAndDelete(self.buffer_dir, album_name, msg)
            print "upload success"
        pass

    def iterativeCrawlAndUpload(self,links):
        self.photoClient.get_albums()
        album_id = self.photoClient.getAlbumId('Auto Backup')
        while True:
           self.photoClient.refresh()
           for i in links:
                album_name,quality,performance = i
                paths=[]
                if(self.client_info['type']=="master"):
                    paths=[quality,performance]
                elif(self.client_info['type']=='worker'):
                    paths=[performance]
                for j in paths:
                    msg = "Link: "+j+"\r\nTime: "+datetime.now().strftime("%Y_%m_%d_%H:%M:%S")+"\r\nUploaded From: "+self.client_info['name']
                    try:
                        self.photoClient.InsertPhoto(urllib.urlopen(j),album_id,album_name+"_"+datetime.now().strftime("%Y_%m_%d_%H:%M:%S")+".jpg",msg)
                    except Exception,Error:
                        print Error
                        print "Link: "+ j
                        pass
           print datetime.now().strftime("%H:%M:%S")+" : Batch complete"


    # def uploadImage(self,arr):
    #     while(True):
    #         self.refreshToken()
    #         self.client.additional_headers = {'Authorization':'Bearer '+self.access_token}
    #         self.get_albums()
    #
    #         for i in arr:
    #             name,quality,performance = i
    #             path = None
    #             if(self.client_info['typ']=='master'):
    #                 path = quality
    #             elif(self.client_info['typ']=='worker'):
    #                 path = performance
    #             try:
    #                 photo = self.InsertPhoto(
    #                     urllib2.urlopen(path),
    #                     name,
    #                     self.getAlbumId('Auto Backup'),
    #                 )
    #                 pass
    #             except Exception,error:
    #                 print(error)
    #                 print("link: "+path)
    #                 pass
    def getLinks(self):
        with open(self.csv_path, 'r') as f:
            Reader = csv.DictReader(f)
            print('Group: ' + str(self.client_info['group']))
            return [(i['Album Name'], i['Quality'], i['Performance']) for i in Reader if
                    i['Group'] == str(self.client_info['group'])]
            # def main(self):
            #     try:
            #         link_arr = self.getLinks("link.csv")
            #     except IOError:
            #         print("No such file")
            #         return
            #     index = 0
            #     threads = []
            #     thread_num = INFO['thread_num']
            #     print(len(link_arr))
            #     for i in range(thread_num):
            #         tmp = [link_arr[index * len(link_arr) / thread_num:((index + 1) * len(link_arr) / thread_num)]]
            #         index += 1
            #         t = Thread(target=GoogleClient.uploadImage, args=(tmp))
            #         t.setDaemon(True)
            #         t.start()
            #         threads.append(t)
            #     print("Main Thread PID:" +str(os.getpid()))
            #     for i in threads:
            #         i.join()
