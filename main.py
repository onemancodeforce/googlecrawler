from PhotoCrawler import PhotoCrawler
import json
import multiprocessing
from threading import Thread
import os
import random
def fork_func(crawler):

    thread_num = crawler.program_info['thread_num']
    try:
        link_arr = crawler.getLinks()
    except IOError:
        print("No such file")
        return
    index = 0
    threads = []
    if(thread_num <2):
        thread_num = 2
    for i in range(thread_num):
        random.shuffle(link_arr)
        t = Thread(target = crawler.iterativeCrawlAndUpload,args = (link_arr,))
        t.start()
        threads.append(t)
    print("Main Thread PID:" + str(os.getpid()))

    threads = [t.join() for t in threads]

def main():
    with open('./config.json','r') as f:
        config=json.load(f)
    crawler = PhotoCrawler(config,"./buffer/")
    process = multiprocessing.Process(target=fork_func, args =(crawler,))
    process.start()
    process.join()
    # now = datetime.now().strftime("%Y_%m_%d_%H:%M:%S")
    # msg = "Uploaded by " + crawler.client_info['name'] + "\r\nUpload Time: " + now
    # crawler.photoClient.get_albums()
    # crawler.photoClient.batchUploadAndDelete(crawler.buffer_dir,'Auto Backup',"Uploaded by Macbook Pro")

main()

